# QUICKScan

<!-- 

- To run the QUICKScan application, download the zip file **runQUICKScan.zip** and unzip it. 
- The batch file **run.bat** under the folder **runQUICKScan/** is ready to run the QUICKScan.
- Update the memory (i.e., the parameter **-Xmx4g**) given in **run.bat** file based on the availability in your laptop. 

-->


The current run.bat is ready to run LARCH.
Update the memory (i.e., the parameter -Xmx4g) given in run.bat file based on the availability in your laptop.

1) To run QUICKScan application, the following command in run.bat file should be uncommented (no :: at the beginning of the line). 
Please provide the correct path of python36 and pcraster in the command.

`java -Xmx4g -classpath lib\QUICKScan.jar nl.wur.app.App "%PYTHONDIR%" "PYTHONPATH=%PYTHONPATH%"`

2) To run LanduseProjector application, the following command in run.bat file should be uncommented (no :: at the beginning of the line):

`java -Xmx4g -classpath lib\QUICKScan.jar nl.wur.landuseprojector.LanduseProjector %1`

The path of the data should be provided while running:
e.g.
run.bat data\iClueCostaRica.prop

3) To run LARCH application, the following command in run.bat file should be uncommented (no :: at the beginning of the line):

`java -Xmx4g -classpath lib\QUICKScan.jar nl.wur.component.larch.Larch %*`

The required parameters should be provided while running.

